const express = require('express') 
const app = express() // inisiasi function express ke varable app
const cors = require('cors') // inisiasi variabel bersi cors
const bodyParser = require('body-parser')
const router = require('../router') // inisiasi router
const morgan = require('morgan') // inisiasi morgan
const YAML = require('yamljs')
const swaggerUI = require('swagger-ui-express')
const apiDocs = YAML.load('./api-doc.yaml')

//loging
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

app.use(cors());
app.use(express.urlencoded({extended : true}))
app.use(bodyParser.json())
app.use('/api', router)
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(apiDocs))

module.exports = app