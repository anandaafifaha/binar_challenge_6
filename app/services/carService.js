const carRepository = require('../repositories/carRepository');

module.exports = {
    async create(requestBody){
        try {
            const car = await carRepository.create(requestBody);

            return {
                data : car,
            }
        } catch (error) {
            throw error;
        }
    },

    async list(){
        try {
            const cars = await carRepository.findAll();

            return{
                data : cars,
            };
        } catch (error) {
            throw error;
        }
    },

    async update(id, requestBody){
        try {
            const car = await carRepository.update(id, requestBody);

            return{
                data : car,
            };
        } catch (error) {
            throw error;
        }
    },

    async delete(id){
        try {
            const car = await carRepository.delete(id);

            return{
                data : car,
            };
        } catch (error) {
            throw error;
        }
    },

    get(id){
        return carRepository.find(id);
    }
}